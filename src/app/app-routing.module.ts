import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { AboutComponent } from './components/about/about.component';
import {AuthGuardGuard} from './_guards/auth-guard.guard';
const routes: Routes = [
  {path: "about" , component : AboutComponent , canActivate: [AuthGuardGuard]},
  { path: '', component: AboutComponent },
  { path: 'login', component: LoginComponent },


  // otherwise redirect to home
  { path: '**', redirectTo: '' }

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
