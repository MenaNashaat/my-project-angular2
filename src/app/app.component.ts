import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './_services/user.service';
import { User } from './_model/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'login-registration';
  //currentUser: User;
  currentUser :User;  
  isCollapse: boolean = false;
  constructor(
    private router: Router,
    private UserService: UserService
    
) {
    this.UserService.currentUser.subscribe(x => {this.currentUser = x; });
}
toggleNavBar() {
  this.isCollapse = !this.isCollapse;
}
logout() {
  this.UserService.logout();
  this.router.navigate(['/login']);
}

}
