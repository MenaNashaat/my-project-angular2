import { Component,  OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { User } from '../../_model/user';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit, OnDestroy {
  currentUser: User;
  currentUserSubscription: Subscription;
  users: User[] = [];
  constructor( private UserService: UserService)  {
    this.currentUserSubscription = this.UserService.currentUser.subscribe(user => {
        this.currentUser = user;
    });
}
ngOnInit() {
 
}
ngOnDestroy() {
  // unsubscribe to ensure no memory leaks
  this.currentUserSubscription.unsubscribe();
}

}
