import { Component, OnInit } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isCollapse = false
  constructor( private UserService: UserService, private Router:Router) { }
  toggleNavBar() {
    this.isCollapse = !this.isCollapse;
  }
  ngOnInit() {
  }
  logout()
  {
   this.UserService.logout();
   this.Router.navigate(['/about']);
  }

}
